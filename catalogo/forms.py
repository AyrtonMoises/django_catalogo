from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import PasswordResetForm

from catalogo.models import Categoria, Produto


class RecuperarSenhaForm(PasswordResetForm):
    email = forms.EmailField(
        label='Email',
        required=True,
        widget=forms.EmailInput(attrs={'class': 'form-control'}),
        help_text='Um email será enviado para sua caixa de entrada'
    )

    def clean_email(self):
        """ Valida email """
        email = self.cleaned_data.get('email')

        email_usuario = User.objects.filter(email=email)
        if not email_usuario:
            raise forms.ValidationError("Não existe um usuário com esse email")
        return email


class CategoriaForm(forms.ModelForm):
    
    class Meta:
        model = Categoria
        fields = '__all__'


class ProdutoForm(forms.ModelForm):
    
    class Meta:
        model = Produto
        exclude = ['datahora_criado','datahora_atualizado','datahora_arquivo', 'status']


