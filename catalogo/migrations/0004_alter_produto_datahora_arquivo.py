# Generated by Django 4.1.7 on 2023-02-28 15:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalogo', '0003_alter_produto_datahora_criado'),
    ]

    operations = [
        migrations.AlterField(
            model_name='produto',
            name='datahora_arquivo',
            field=models.DateTimeField(null=True),
        ),
    ]
