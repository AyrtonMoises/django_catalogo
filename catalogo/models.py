from django.db import models


class Categoria(models.Model):
    descricao = models.CharField(max_length=100)

    def __str__(self):
        return self.descricao


class Produto(models.Model):
    descricao = models.CharField(max_length=150)
    foto = models.ImageField()
    detalhes = models.TextField(blank=True)
    categoria = models.ForeignKey(Categoria, on_delete=models.PROTECT, related_name="produto_categoria")
    preco = models.DecimalField(max_digits=5, decimal_places=2)
    estoque = models.IntegerField()
    datahora_criado = models.DateTimeField(auto_now_add=True)
    datahora_atualizado = models.DateTimeField(auto_now=True)
    datahora_arquivo = models.DateTimeField(null=True)
    status = models.BooleanField(verbose_name='Inativo', default=False)


