from django.urls import path
from catalogo.views import (
    login, logout, RedefinirSenhaView, 
    RedefinirSenhaConfirmarView, ProdutosCategoriaListView, CategoriaCreateView, ProdutoCreateView,
    ProdutosListView, CategoriaListView, ProdutoDetailView, CategoriaDetailView, ProdutoArquivaView,
    ProdutoUpdateView, CategoriaUpdateView
)


urlpatterns = [
    path('', ProdutosCategoriaListView.as_view(), name="home"),
    path('login/', login, name="login"),
    path('logout/', logout, name="logout"),
    path('redefinir-senha/', RedefinirSenhaView.as_view() ,name="redefinir_senha"),
    path('confirmar-redefinir-senha/<uidb64>/<token>/',
        RedefinirSenhaConfirmarView.as_view(),
        name='confirmar_redefinir_senha'
    ),
    path('cadastro-categoria/', CategoriaCreateView.as_view(), name="cadastro_categoria"),
    path('cadastro-produto/', ProdutoCreateView.as_view(), name="cadastro_produto"),
    path('editar-produto/<int:pk>/', ProdutoUpdateView.as_view(), name="editar_produto"),
    path('editar-categoria/<int:pk>/', CategoriaUpdateView.as_view(), name="editar_categoria"),
    path('lista-produtos/', ProdutosListView.as_view(), name="lista_produtos"),
    path('lista-categorias/', CategoriaListView.as_view(), name="lista_categorias"),
    path('produto-detail/<int:pk>/', ProdutoDetailView.as_view(), name="produto_detail"),
    path('categoria-detail/<int:pk>/', CategoriaDetailView.as_view(), name="categoria_detail"),
    path('arquiva-produto/<int:pk>/', ProdutoArquivaView.as_view(), name="arquiva_produto"),
]
