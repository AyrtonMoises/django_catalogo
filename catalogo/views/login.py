from django.contrib import auth, messages
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.views import (
    PasswordResetView, PasswordResetConfirmView, PasswordResetDoneView
)
from catalogo.forms import RecuperarSenhaForm



def login(request):
    """ Caso esteja logado redireciona a home """
    if request.method == 'GET':
        if request.user.is_authenticated:
            return redirect('home')

    """ Realiza login do usuario """
    if request.method == 'POST':
        username = request.POST['username']
        senha = request.POST['senha']

        if username == "" or senha == "":
            messages.error(request, "Campos email e senha não podem ficar em branco!")
            return redirect('login')
        user = auth.authenticate(request, username=username, password=senha)

        if user is not None:
            auth.login(request, user)
            return redirect('home')
        else:
            messages.error(request, "Username/Senha estão incorretos")
            return render(request, 'login.html')

    return render(request, 'login.html')


def logout(request):
    """ Realiza logout do usuário """
    auth.logout(request)
    return redirect('login')


class RedefinirSenhaView(SuccessMessageMixin, PasswordResetView):
    """ Envia email de recuperação de senha """
    template_name = 'redefinicao-senha/redefinir-senha.html'
    form_class = RecuperarSenhaForm
    email_template_name = 'redefinicao-senha/template-redefinir-senha.html'
    subject_template_name = 'redefinicao-senha/assunto-redefinir-senha.txt'
    success_message = "Um email foi enviado para sua caixa de entrada." \
                      "Verifique também sua caixa de spam antes de tentar novamente." 
    success_url = reverse_lazy('login')

class RedefinirSenhaConfirmarView(SuccessMessageMixin, PasswordResetConfirmView):
    """ Redefinir senha """
    template_name='redefinicao-senha/confirmar-redefinir-senha.html'
    success_url=reverse_lazy('login')
    success_message = "Senha alterada com sucesso!"
