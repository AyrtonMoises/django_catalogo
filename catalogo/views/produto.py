from datetime import datetime 

from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.list import ListView
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic.detail import DetailView
from django.views import View
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from django.core.paginator import Paginator

from catalogo.models import Produto, Categoria
from catalogo.forms import ProdutoForm, CategoriaForm


class ProdutosCategoriaListView(LoginRequiredMixin, TemplateView):
    template_name = 'listagem.html'


class ProdutosListView(LoginRequiredMixin, ListView):
    template_name = '_produtos.html'
    context_object_name = 'produtos'
    paginate_by = 5

    def get_queryset(self):
        return Produto.objects.all().order_by('-datahora_criado')


class CategoriaListView(LoginRequiredMixin, ListView):
    template_name = '_categorias.html'
    context_object_name = 'categorias'
    model = Categoria
    paginate_by = 5

class ProdutoCreateView(SuccessMessageMixin, LoginRequiredMixin, CreateView):
    template_name = 'form-produto.html'
    form_class = ProdutoForm
    success_url = reverse_lazy('home')
    success_message = "Produto cadastrado com sucesso"

    def form_valid(self, form):
        if form.instance.status == 1:
            form.instance.datahora_arquivo = datetime.now()
        else:
            form.instance.datahora_arquivo = None
        return super().form_valid(form)

    def form_invalid(self, form):
        response = super().form_invalid(form)
        response.status_code = 400
        return response

class ProdutoUpdateView(SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    template_name = 'edit-produto.html'
    form_class = ProdutoForm
    success_message = "Produto atualizado com sucesso!"
    model = Produto
    success_url = reverse_lazy('home')

class CategoriaCreateView(SuccessMessageMixin, LoginRequiredMixin, CreateView):
    template_name = 'form-categoria.html'
    form_class = CategoriaForm
    success_url = reverse_lazy('home')
    success_message = "Categoria cadastrado com sucesso"

    def form_invalid(self, form):
        response = super().form_invalid(form)
        response.status_code = 400
        return response

class CategoriaUpdateView(SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    template_name = 'edit-categoria.html'
    form_class = CategoriaForm
    success_message = "Categoria atualizada com sucesso!"
    model = Categoria
    success_url = reverse_lazy('home')


class ProdutoDetailView(LoginRequiredMixin, DetailView):
    template_name = 'produto_detail.html'
    context_object_name = 'produto'
    model = Produto

class CategoriaDetailView(LoginRequiredMixin, DetailView):
    template_name = 'categoria_detail.html'
    context_object_name = 'categoria'
    model = Categoria


class ProdutoArquivaView(LoginRequiredMixin, View):
    
    def post(self, request, *args, **kwargs):
        id_produto = kwargs['pk']
        produto = get_object_or_404(Produto, pk=id_produto)

        if produto.status:
            produto.status = 0
            produto.datahora_arquivo = None
        else:
            produto.status = 1
            produto.datahora_arquivo = datetime.now()

        produto.save()

        return HttpResponse('ok')